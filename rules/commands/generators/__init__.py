import json
import csv
from random import *

end_char = chr(0) # End of text in prob files.

def _prob_file(wordlist_file):
	return f"{wordlist_file}.prob.json"


def init_prob_file(wordlist_file, state_len, prob_file=None):
	"""Generate the probability file that can be used to generate random words

	Args:
		wordlist_file (str): The wordlist file to read from
		state_len (int): The length of the state to use in the probability generation
		prob_file (str, optional): The name of the probability file to generate. Defaults to None.
	"""
	initializer = _ProbabilityInitializer.from_wordlist(wordlist_file, state_len)
	print(prob_file)

	# Bug in cli-util doesn't handle None defaults correctly
	if prob_file is None or prob_file == 'None':
		print("prob_file is None")
		prob_file = _prob_file(wordlist_file)
	print(prob_file)
	initializer.to_prob_file(prob_file)

def generate_word(prob_file):
	"""Generate a word, based on the wordlist used to generate the probability file.

	Args:
		prob_file (str): The probability file to use.
	"""
	generator = MarkovGenerator.from_prob_file(prob_file)
	print(generator.generate_word())

class _ProbabilityInitializer():
	def __init__(self):
		self.probability = {}

	def add_word(self, word, state_len):
		state = ''
		cur_state = ''

		for char in word + end_char:
			state += char
			if len(state) > state_len:
				state = state[-state_len:]

			if cur_state not in self.probability:
				self.probability[cur_state] = {}

			if state not in self.probability[cur_state]:
				self.probability[cur_state][state] = 1
			else:
				self.probability[cur_state][state] += 1

			cur_state = state

	def normalize(self):
		for i in self.probability:
			total = 0
			for j in self.probability[i]:
				total += self.probability[i][j]

			for j in self.probability[i]:
				self.probability[i][j] = self.probability[i][j] / total

	def to_prob_file(self, prob_file):
		with open(prob_file, 'w') as f:
			json.dump(self.probability, f, indent=2)

	@staticmethod
	def from_wordlist(wordlist_file, state_len):
		initializer = _ProbabilityInitializer()

		with open(wordlist_file, newline='') as csvfile:
			prob_reader = csv.reader(csvfile, delimiter=',', quotechar='|')
			for row in prob_reader:
				word = row[0]
				initializer.add_word(word, state_len)
		initializer.normalize()
		return initializer


class MarkovGenerator():

	def __init__(self, probabilities):
		self.probability = probabilities

	def generate_word(self):
		generated_word = ""
		current_state = ""
		while len(generated_word) == 0 or generated_word[-1] != end_char:
			state_probs = self.probability[current_state]
			r = random()
			total = 0
			for key in state_probs:
				total += state_probs[key]
				if total >= r:
					current_state = key
					generated_word += key[-1]
					break

		return generated_word[:-1]


	@staticmethod
	def from_prob_file(prob_file):
		with open(prob_file) as f:
			return MarkovGenerator(json.load(f))
