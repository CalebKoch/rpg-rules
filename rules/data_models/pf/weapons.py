"""Rpg rules site generator

Copyright (C) 2021 Caleb Koch

This program is free software: you can redistribute it and/or modify
it under the terms of version 3 of the GNU Affero General Public License as
published by the Free Software Foundation.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""

from copy import copy
from ..equipment import Weapon
from ...data_readers import read_dataclass

def read_custom_weapons(weapon_dict: dict):
    if isinstance(weapon_dict, str):
        weapon_dict = {'data_source': weapon_dict}
    else:
        weapon_dict = copy(weapon_dict)
    enhancement = None
    masterwork = False
    try:
        enhancement = weapon_dict.pop('enhancement')
        to_hit = weapon_dict.get('to_hit', dict())
        to_hit.setdefault('enhancement', enhancement)
        weapon_dict['to_hit'] = to_hit
        damage = weapon_dict.get('damage', dict())
        damage.setdefault('enhancement', enhancement)
        weapon_dict['damage'] = damage
    except KeyError:
        # If enhancement doesn't exist, no problem.
        pass

    try:
        if not enhancement:
            masterwork = weapon_dict.pop('masterwork')
        if masterwork:
            to_hit = weapon_dict.get('to_hit', dict())
            to_hit.setdefault('enhancement', 1)
            weapon_dict['to_hit'] = to_hit
    except KeyError:
        pass

    weapon = read_dataclass(Weapon, weapon_dict)

    weapon_name = weapon.name
    enhancement_cost = enhancement
    static_costs = 0
    if weapon.weapon_ability:
        for ability in weapon.weapon_ability:
            if ability.bonus_cost:
                enhancement_cost += ability.bonus_cost
            if ability.price:
                static_costs += ability.price
            weapon.name = f"{ability.name} {weapon.name}"

    if enhancement:
        weapon.name = f"+{enhancement} {weapon.name}"
        if 'cost' not in weapon_dict:
            # Make sure it wasn't overridden before we try changing it.
            # 300 for masterwork quality.
            weapon.cost += (enhancement_cost * enhancement_cost * 2000) + 300
            weapon.cost = weapon.cost + static_costs
    elif masterwork:
        weapon.name = f"Masterwork {weapon.name}"
        if 'cost' not in weapon_dict:
            # Make sure i wasn't overridden before we try changing it.
            # 300 for masterwork quality.
            weapon.cost = weapon.cost + 300

    return weapon
