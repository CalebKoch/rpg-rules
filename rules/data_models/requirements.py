from dataclasses import dataclass, field
from typing import List
from decimal import Decimal

@dataclass
class Requirements:
    feats: List[str] = field(default_factory=list)
    spells: List[str] = field(default_factory=list)
    special: List[str] = field(default_factory=list)
    cost: Decimal = 0
