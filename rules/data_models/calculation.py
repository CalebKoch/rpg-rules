from typing import Dict, Union

class Calculation:
    """Represents a calculated value. Allows seeing how the value was generated, as well as the
    total.
    """
    modifiers: Dict[str, Union[str, int]]
    first_key: str

    def __init__(self, modifiers: Dict[str, int], first_key: str=None):
        # If they give us the total, they shouldn't give us anything else.
        self.first_key = first_key
        self.modifiers = modifiers


    @property
    def total(self):
        """The total for the calculation."""
        int_total = 0
        str_total = ""

        if self.first_key in self.modifiers:
            value = self.modifiers[self.first_key]
            try:
                int_total += int(value)
            except ValueError:
                str_total += value + '+'
        if 'ability' in self.modifiers and self.first_key != 'ability':
            value = self.modifiers['ability']
            try:
                int_total += int(value)
            except ValueError:
                str_total += value + '+'

        for key, value in self.modifiers.items():
            if key == self.first_key or key == 'ability':
                continue
            try:
                int_total += int(value)
            except ValueError:
                str_total += value + '+'

        if not str_total:
            return int_total
        if not int_total:
            return str_total.strip(" +")
        return str_total + str(int_total)


    @property
    def expanded(self):
        expanded = ""
        first = True

        # In some cases there is a specific key that makes sense to go first,
        # such as ranks for skills.
        if self.first_key in self.modifiers:
            expanded = f"{self.modifiers[self.first_key]} ({self.first_key})"
            first = False
        # Ability is usually the next most relevant factor.
        if 'ability' in self.modifiers and self.first_key != 'ability':
            if not first:
                expanded += " + "
            expanded += f"{self.modifiers['ability']} (ability)"
            first = False

        for key, value in self.modifiers.items():
            if key == self.first_key or key == 'ability':
                continue
            if not first:
                expanded += " + "
            expanded += f"{value} ({key})"
            first = False

        return expanded

    def __str__(self):
        return str(self.total)

    @classmethod
    def create(
            cls,
            modifiers: Dict[str, int],
            default_values: Dict[str, int],
            character_values: Dict[str, int],
            first_key:str=None):
        if not isinstance(modifiers, dict):
            return cls({'total': modifiers})

        if "total" in modifiers:
            if len(modifiers) != 1:
                raise ValueError("Cannot set modifiers and total.")
            return cls(modifiers)

        modifiers = default_values | modifiers

        for key, value in modifiers.items():
            if isinstance(value, str):
                modifiers[key] = Calculation.parse_value(value, character_values)

        return cls(modifiers, first_key=first_key)


    @staticmethod
    def parse_value(value: str, character_values: Dict[str, int]):
        if '+' in value:
            summands = value.split('+')
            total = 0
            for summand in summands:
                total += Calculation.parse_value(summand, character_values)
            return total
        elif '/' in value:
            parts = value.rsplit('/', 1)
            dividend = Calculation.parse_value(parts[0], character_values)
            divisor = Calculation.parse_value(parts[1], character_values)
            return int(dividend / divisor)
        elif '*' in value:
            parts = value.rsplit('*', 1)
            multiplicand = Calculation.parse_value(parts[0], character_values)
            multiplier = Calculation.parse_value(parts[1], character_values)
            return multiplicand * multiplier

        trimmed = value.strip().lower()
        if trimmed in character_values:
            return character_values[trimmed]

        try:
            return int(trimmed)
        except ValueError:
            return value
