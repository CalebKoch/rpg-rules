import asyncio
import logging

import os
import re

from markdown import markdown
from nio.events import RoomMessageText
from nio.client import AsyncClient
from nio.rooms import MatrixRoom

from .commands import search
from .logging import init_logging

init_logging()

logger = logging.getLogger(__name__)

class MissingEnvironmentVariable(Exception):
	pass

def get_required_env_variable(variable: str) -> str:
	value = os.getenv(variable)
	if value is None:
		error_msg = f"Missing {variable} env variable."
		logger.error(error_msg)
		raise MissingEnvironmentVariable(error_msg)

	return value
		
PREFIX = os.getenv("COMMAND_PREFIX") or "!"
USERNAME = get_required_env_variable("MATRIX_USERNAME")
HOMESERVER = get_required_env_variable("MATRIX_HOMESERVER")
PASSWORD = get_required_env_variable("MATRIX_PASSWORD")
STORE_PATH = os.getenv("STORE_PATH") or './store'

if len(PREFIX) > 1:
	logger.warning("Using a PREFIX longer than a single character may not work correctly.")

regex_string = rf"\[\{PREFIX}([^]]*)\]"
COMMAND_REGEX = re.compile(rf"\[\{PREFIX}([^]]*)\]")


client = AsyncClient(
	HOMESERVER, 
	USERNAME,
	store_path=STORE_PATH,
)

async def check_message(room: MatrixRoom, message: RoomMessageText) -> None:
	logger.debug(f"recieved message {message.body}")
	match = COMMAND_REGEX.findall(message.body)

	if not match:
		return

	for current in match:
		result = search(current.strip())
		logger.debug(result)

		if result:
			content = {
				"msgtype": "m.notice",
				"body": result,
				"format": "org.matrix.custom.html",
				"formatted_body": markdown(result),
				"m.relates_to": {"m.in_reply_to": {"event_id": message.event_id}},
			}

			await client.room_send(
			# Watch out! If you join an old room you'll see lots of old messages
				room_id=room.room_id,
				message_type="m.room.message",
				content=content
			)

async def start_async():

	client.add_event_callback(check_message, RoomMessageText)
	logger.info(await client.login(PASSWORD))
	# "Logged in as @alice:example.org device id: RANDOMDID"

	# If you made a new room and haven't joined as that user, you can use
	# await client.join("your-room-id")

	await client.sync_forever(timeout=30000)  # milliseconds

def start():
	asyncio.run(start_async())
