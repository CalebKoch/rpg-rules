name: Brawler
sources:
  - pathfinder-roleplaying-game-advanced-class-guide
tags: []
max_level: 20
hit_die: 10
starting_wealth: 3d6 × 10 gp (average 105 gp)
class_skills:
  - Acrobatics
  - Climb
  - Craft
  - Escape Artist
  - Handle Animal
  - Intimidate
  - Knowledge (dungeoneering)
  - Knowledge (local)
  - Perception
  - Profession
  - Ride
  - Sense Motive
  - Swim
skill_ranks_per_level: 4
bab_progression: full
fort_progression: good
ref_progression: good
will_progression: poor
weapon_proficiencies:
  - Simple
  - handaxe
  - shortsword
  - weapons from the close fighter weapon group
armor_proficiencies:
  - Light
shield_proficiencies:
  - Shields
class_features:
  - name: Brawler’s Cunning
    type: Ex
    levels:
      - 1
    text: |
      If the brawler’s Intelligence score is less than 13, it counts as 13 for
      the purpose of meeting the prerequisites of combat feats.
  - name: Martial Flexibility
    type: Ex
    levels:
      - 1
      - 6
      - 10
      - 12
      - 20
    levels_text:
      6: (swift action)
      10: (free action)
      12: (immediate action)
      20: (any number)
    text: |
      A brawler can take a move action to gain the benefit of a combat feat she
      doesn’t possess. This effect lasts for 1 minute. The brawler must meet
      all the feat’s prerequisites. She may use this ability a number of times
      per day equal to 3 + 1/2 her brawler level (minimum 1).

      The brawler can use this ability again before the duration expires in
      order to replace the previous combat feat with another choice.

      If a combat feat has a daily use limitation (such as Stunning Fist), any
      uses of that combat feat while using this ability count toward that
      feat’s daily limit.

      At 6th level, a brawler can use this ability to gain the benefit of two
      combat feats at the same time. She may select one feat as a swift action
      or two feats as a move action. She may use one of these feats to meet a
      prerequisite of the second feat; doing so means that she cannot replace a
      feat currently fulfilling another’s prerequisite without also replacing
      those feats that require it. Each individual feat selected counts toward
      her daily uses of this ability.

      At 10th level, a brawler can use this ability to gain the benefit of
      three combat feats at the same time. She may select one feat as a free
      action, two feats as a swift action, or three feats as a move action. She
      may use one of the feats to meet a prerequisite of the second and third
      feats, and use the second feat to meet a prerequisite of the third feat.
      Each individual feat selected counts toward her daily uses of this
      ability.

      At 12th level, a brawler can use this ability to gain the benefit of one
      combat feat as an immediate action or three combat feats as a swift
      action. Each individual feat selected counts toward her daily uses of
      this ability.

      At 20th level, a brawler can use this ability to gain the benefit of any
      number of combat feats as a swift action. Each feat selected counts
      toward her daily uses of this ability.
  - name: Martial Training
    type: Ex
    levels:
      - 1
    text: |
      At 1st level, a brawler counts her total brawler levels as both fighter
      levels and monk levels for the purpose of qualifying for feats. She also
      counts as both a fighter and a monk for feats and magic items that have
      different effects based on whether the character has levels in those
      classes (such as Stunning Fist and a monk’s robe). This ability does not
      automatically grant feats normally granted to fighters and monks based on
      class level, namely Stunning Fist.
  - name: Unarmed Strike
    levels:
      - 1
    text: |
      At 1st level, a brawler gains Improved Unarmed Strike as a bonus feat. A
      brawler may attack with fists, elbows, knees, and feet. This means that a
      brawler may make unarmed strikes with her hands full. A brawler applies
      her full Strength modifier (not half ) on damage rolls for all her
      unarmed strikes.

      Usually, a brawler’s unarmed strikes deal lethal damage, but she can
      choose to deal nonlethal damage instead with no penalty on her attack
      roll. She has the same choice to deal lethal or nonlethal damage while
      grappling.

      A brawler’s unarmed strike is treated as both a manufactured weapon and a
      natural weapon for the purpose of spells and effects that modify either
      manufactured weapons or natural weapons.

      A brawler also deals more damage with her unarmed strikes than others, as
      shown on Table: Brawler. The unarmed damage values listed on that table
      are for Medium brawlers. A Small brawler deals less damage than the
      amount given there with her unarmed attacks, while a Large brawler deals
      more damage; see the following table. Table: Small or Large Brawler
      Unarmed Damage

      | Level     | Small Brawler | Large Brawler |
      |:----------|:-------------:|:-------------:|
      | 1st-3rd   |      1d4      |      1d8      |
      | 4th-7th   |      1d6      |      2d6      |
      | 8th-11th  |      1d8      |      2d8      |
      | 12th-15th |     1d10      |      3d6      |
      | 16th-19th |      2d6      |      3d8      |
      | 20th      |      2d8      |      4d8      |
  - name: Bonus Combat Feats
    levels:
      - 2
      - 5
      - 8
      - 11
      - 14
      - 17
      - 20
    text: |
      At 2nd level and every 3 levels thereafter, a brawler gains a bonus
      combat feat in addition to those gained from normal advancement. These
      bonus feats must be ones that affect or improve her defenses or melee
      attacks. The brawler must meet the prerequisites of the selected bonus
      combat feat.

      Upon reaching 5th level and every 3 levels thereafter, a brawler can
      choose to learn a new bonus combat feat in place of a bonus combat feat
      she has already learned. In effect, the brawler loses the bonus combat
      feat in exchange for the new one. The old feat cannot be one that was
      used as a prerequisite for another feat, prestige class, or other
      ability. A brawler can only change one feat at any given level, and must
      choose whether or not to swap the feat at the time she gains a new bonus
      combat feat for the level.
  - name: Brawler’s Flurry
    type: Ex
    levels:
      - 2
      - 8
      - 15
    levels_text:
      2: (Two-Weapon Fighting)
      8: (Improved Two-Weapon Fighting)
      15: (Greater Two-Weapon Fighting)
    text: |
      Starting at 2nd level, a brawler can make a brawler’s flurry as a
      full-attack action. When doing so, a brawler has the Two-Weapon Fighting
      feat when attacking with any combination of unarmed strikes, weapons from
      the close fighter weapon group, or weapons with the “monk” special
      feature. She does not need to use two different weapons to use this
      ability.

      A brawler applies her full Strength modifier to her damage rolls for all
      attacks made with brawler’s flurry, whether the attacks are made with an
      off-hand weapon or a weapon wielded in both hands. A brawler can
      substitute disarm, sunder, and trip combat maneuvers for unarmed attacks
      as part of brawler’s flurry. A brawler with natural weapons can’t use
      such weapons as part of brawler’s flurry, nor can she make natural weapon
      attacks in addition to her brawler’s flurry attacks.

      At 8th level, the brawler gains use of the Improved Two-Weapon Fighting
      feat when using brawler’s flurry. At 15th level, she gains use of the
      Greater Two-Weapon Fighting feat when using brawler’s flurry.
  - name: Maneuver Training
    type: Ex
    levels:
      - 3
      - 7
      - 11
      - 15
      - 19
    levels_text:
      3: 1
      7: 2
      11: 3
      15: 4
      19: 5
    text: |
      At 3rd level, a brawler can select one combat maneuver to receive
      additional training. She gains a +1 bonus on combat maneuver checks when
      performing that combat maneuver and a +1 bonus to her CMD when defending
      against that maneuver.

      At 7th level and every 4 levels thereafter, the brawler becomes further
      trained in another combat maneuver, gaining the above +1 bonus combat
      maneuver checks and to CMD. In addition, the bonuses granted by all
      previous maneuver training increase by 1 each. (For example, if a brawler
      chooses grapple at 3rd level and sunder at 7th level, her bonuses to
      grapple are +2 and bonuses to sunder are +1. If she then chooses bull
      rush upon reaching 11th level, her bonuses to grapple are +3, to sunder
      are +2, and to bull rush are +1.)
  - name: AC Bonus
    type: Ex
    levels:
      - 4
      - 9
      - 13
      - 18
    levels_text:
      4: "+1"
      9: "+2"
      13: "+3"
      18: "+4"
    text: |
      At 4th level, when a brawler wears light or no armor, she gains a +1
      dodge bonus to AC and CMD. This bonus increases by 1 at 9th, 13th, and
      18th levels.

      These bonuses to AC apply against touch attacks. She loses these bonuses
      while immobilized or helpless, wearing medium or heavy armor, or carrying
      a medium or heavy load.
  - name: Knockout
    type: Ex
    levels:
      - 4
      - 10
      - 16
    levels_text:
      4: 1/day
      10: 2/day
      16: 3/day
    text: |
      At 4th level, once per day a brawler can unleash a devastating attack
      that can instantly knock a target unconscious. She must announce this
      intent before making her attack roll. If the brawler hits and the target
      takes damage from the blow, the target must succeed at a Fortitude saving
      throw (DC = 10 + 1/2 the brawler’s level + the higher of the brawler’s
      Strength or Dexterity modifier) or fall unconscious for 1d6 rounds. Each
      round on its turn, the unconscious target may attempt a new saving throw
      to end the effect as a full-round action that does not provoke attacks of
      opportunity. Creatures immune to critical hits or nonlethal damage are
      immune to this ability. At 10th level, the brawler may use this ability
      twice per day; at 16th level, she may use it three times per day.
  - name: Brawler’s Strike
    type: Ex
    levels:
      - 5
      - 9
      - 12
      - 17
    levels_text:
      5: (magic)
      9: (cold iron)
      12: (alignment)
      17: (adamantine)
    text: |
      At 5th level, a brawler’s unarmed strikes are treated as magic weapons
      for the purpose of overcoming damage reduction. At 9th level, her unarmed
      attacks are also treated as cold iron and silver for the purpose of
      overcoming damage reduction. At 12th level, she chooses one alignment
    text: |
      At 5th level, a brawler’s unarmed strikes are treated as magic weapons
      for the purpose of overcoming damage reduction. At 9th level, her unarmed
      attacks are also treated as cold iron and silver for the purpose of
      overcoming damage reduction. At 12th level, she chooses one alignment
      component: chaotic, evil, good, or lawful; her unarmed strikes also count
      as this alignment for the purpose of overcoming damage reduction. (This
      alignment component cannot be the opposite of the brawler’s actual
      alignment, such as a good brawler choosing evil strikes.) At 17th level,
      her unarmed attacks are also treated as adamantine weapons for the
      purpose of overcoming damage reduction and bypassing hardness.
  - name: Close Weapon Mastery
    type: Ex
    levels:
      - 5
    text: |
      At 5th level, a brawler’s damage with close weapons increases. When
      wielding a close weapon, she uses the unarmed strike damage of a brawler
      4 levels lower instead of the base damage for that weapon (for example, a
      5th-level Medium brawler wielding a punching dagger deals 1d6 points of
      damage instead of the weapon’s normal 1d4). If the weapon normally deals
      more damage than this, its damage is unchanged. This ability does not
      affect any other aspect of the weapon. The brawler can decide to use the
      weapon’s base damage instead of her adjusted unarmed strike damage—this
      must be declared before the attack roll is made.
  - name: Awesome Blow
    type: Ex
    levels:
      - 16
    text: |
      At 16th level, the brawler can as a standard action perform an awesome
      blow combat maneuver against a corporeal creature of her size or smaller.
      If the combat maneuver check succeeds, the opponent takes damage as if
      the brawler hit it with the close weapon she is wielding or an unarmed
      strike, it is knocked flying 10 feet in a direction of the brawler’s
      choice, and it falls prone. The brawler can only push the opponent in a
      straight line, and the opponent can’t move closer to the brawler than the
      square it started in. If an obstacle prevents the completion of the
      opponent’s move, the opponent and the obstacle each take 1d6 points of
      damage, and the opponent is knocked prone in the space adjacent to the
      obstacle. (Unlike the Awesome Blow monster feat, the brawler can be of
      any size to use this ability.)
  - name: Improved Awesome Blow
    type: Ex
    levels:
      - 20
    text: |
      At 20th level, the brawler can use her awesome blow ability as an attack
      rather than as a standard action. She may use it on creatures of any
      size. If the maneuver roll is a natural 20, the brawler can immediately
      attempt to confirm the critical by rolling another combat maneuver check
      with all the same modifiers as the one just rolled; if the confirmation
      roll is successful, the attack deals double damage, and the damage from
      hitting an obstacle (if any) is also doubled.
text: |
  Deadly even with nothing in her hands, a brawler eschews using the fighter’s
  heavy armor and the monk’s mysticism, focusing instead on perfecting many
  styles of brutal unarmed combat. Versatile, agile, and able to adapt to most
  enemy attacks, a brawler’s body is a powerful weapon.

  **Role:** Brawlers are maneuverable and well suited for creating flanking
  situations or dealing with lightly armored enemies, as well as quickly
  adapting to a rapidly changing battlefield.

  **Alignment:** Any.
