name: Ranger
sources:
  - pathfinder-roleplaying-game-core-rulebook
tags: []
max_level: 20
hit_die: 10
prerequisites:
  - Any
starting_wealth: 5d6 × 10 gp (average 175 gp.)
class_skills:
  - Climb
  - Craft
  - Handle Animal
  - Heal
  - Intimidate
  - Knowledge (dungeoneering)
  - Knowledge (geography)
  - Knowledge (nature)
  - Perception
  - Profession
  - Ride
  - Spellcraft
  - Stealth
  - Survival
  - Swim
skill_ranks_per_level: 6
bab_progression: full
fort_progression: good
ref_progression: good
will_progression: poor
spells_per_day: ranger
spell_list: ranger
spell_list_levels: 4
class_features:
  - name: Favored Enemy
    type: Ex
    levels:
      - 1
      - 5
      - 10
      - 15
      - 20
    levels_text:
      1: 1st
      5: 2nd
      10: 3rd
      15: 4th
      20: 5th
    text: |
      At 1st level, a ranger selects a creature type from the ranger favored
      enemies table. He gains a +2 bonus on Bluff, Knowledge, Perception, Sense
      Motive, and Survival checks against creatures of his selected type.
      Likewise, he gets a +2 bonus on weapon attack and damage rolls against
      them. A ranger may make Knowledge skill checks untrained when attempting
      to identify these creatures. At 5th level and every five levels
      thereafter (10th, 15th, and 20th level), the ranger may select an
      additional favored enemy. In addition, at each such interval, the bonus
      against any one favored enemy (including the one just selected, if so
      desired) increases by +2. If the ranger chooses humanoids or outsiders as
      a favored enemy, he must also choose an associated subtype, as indicated
      on the table below. (Note that there are other types of humanoid to
      choose from—those called out specifically on the table below are merely
      the most common.) If a specific creature falls into more than one
      category of favored enemy, the ranger’s bonuses do not stack; he simply
      uses whichever bonus is higher.
  - name: Track
    type: Ex
    levels:
      - 1
    text: |
      A ranger adds half his level (minimum 1) to Survival skill checks made to
      follow tracks.
  - name: Wild Empathy
    type: Ex
    levels:
      - 1
    text: |
      A ranger can improve the initial attitude of an animal. This ability
      functions just like a Diplomacy check to improve the attitude of a person
      (see Using Skills). The ranger rolls 1d20 and adds his ranger level and
      his Charisma bonus to determine the wild empathy check result. The
      typical domestic animal has a starting attitude of indifferent, while
      wild animals are usually unfriendly. To use wild empathy, the ranger and
      the animal must be within 30 feet of one another under normal visibility
      conditions. Generally, influencing an animal in this way takes 1 minute,
      but, as with influencing people, it might take more or less time. The
      ranger can also use this ability to influence a magical beast with an
      Intelligence score of 1 or 2, but he takes a –4 penalty on the check.
  - name: Combat Style Feat
    type: Ex
    levels:
      - 2
      - 6
      - 10
      - 14
      - 18
    text: |
      At 2nd level, a ranger must select one combat style to pursue. The
      ranger’s expertise manifests in the form of bonus feats at 2nd, 6th,
      10th, 14th, and 18th level. He can choose feats from his selected combat
      style, even if he does not have the normal prerequisites. The benefits of
      the ranger’s chosen style feats apply only when he wears light, medium,
      or no armor. He loses all benefits of his combat style feats when wearing
      heavy armor. Once a ranger selects a combat style, it cannot be changed.
  - name: Endurance
    levels:
      - 3
    text: A ranger gains Endurance as a bonus feat at 3rd level.
  - name: Favored Terrain
    type: Ex
    levels:
      - 3
      - 8
      - 13
      - 18
    levels_text:
      3: 1st
      8: 2nd
      13: 3rd
      18: 4th
    text: |
      At 3rd level, a ranger may select a type of terrain from Table: Ranger
      Favored Terrains. The ranger gains a +2 bonus on initiative checks and
      Knowledge (geography), Perception, Stealth, and Survival skill checks
      when he is in this terrain. A ranger traveling through his favored
      terrain normally leaves no trail and cannot be tracked (though he may
      leave a trail if he so chooses). At 8th level and every five levels
      thereafter, the ranger may select an additional favored terrain. In
      addition, at each such interval, the skill bonus and initiative bonus in
      any one favored terrain (including the one just selected, if so desired),
      increases by +2. If a specific terrain falls into more than one category
      of favored terrain, the ranger’s bonuses do not stack; he simply uses
      whichever bonus is higher.
  - name: Hunter’s Bond
    type: Ex
    levels:
      - 4
    text: |
      At 4th level, a ranger forms a bond with his hunting companions. This
      bond can take one of two forms. Once the form is chosen, it cannot be
      changed. The first is a bond to his companions. This bond allows him to
      spend a move action to grant half his favored enemy bonus against a
      single target of the appropriate type to all allies within 30 feet who
      can see or hear him. This bonus lasts for a number of rounds equal to the
      ranger’s Wisdom modifier (minimum 1). This bonus does not stack with any
      favored enemy bonuses possessed by his allies; they use whichever bonus
      is higher. The second option is to form a close bond with an animal
      companion. A ranger who selects an animal companion can choose from the
      following list: badger, bird, camel, cat (small), dire rat, dog, horse,
      pony, snake (viper or constrictor), or wolf. If the campaign takes place
      wholly or partly in an aquatic environment, the ranger may choose a shark
      instead. This animal is a loyal companion that accompanies the ranger on
      his adventures as appropriate for its kind. A ranger’s animal companion
      shares his favored enemy and favored terrain bonuses. This ability
      functions like the druid animal companion ability (which is part of the
      Nature Bond class feature), except that the ranger’s effective druid
      level is equal to his ranger level –3. A ranger can select a
      antelopePPC:WO, baboonPPC:WO, bustard, capybara, elkPPC:WO, falcon,
      kangarooPPC:WO, lizard (giant gecko)PPC:WO, marsupial devil, ramPPC:WO,
      reindeer, snake (reef snake or spitting cobra), stagPPC:WO, trumpeter
      swanPPC:WO, thylacinePPC:WO, wolfdog, yak, or zebra as an animal
      companion. If the campaign takes place in an aquatic environment, the
      ranger can choose an armorfish or reef snake. A falconer ranger can
      select a falcon companion instead of a bird companion. PZO1140 If the
      campaign takes place in an aquatic environment, the ranger can choose a
      stingrayPPC:WO. The ambusher, bully, daredevil, feytouched companion,
      precocious companion, totem guide, tracker, verdant companion, and
      wrecker archetypes are all particularly appropriate for a ranger’s animal
      companion.
  - name: Spells
    text: |
      Beginning at 4th level, a ranger gains the ability to cast a small number
      of divine spells, which are drawn from the ranger spell list. A ranger
      must choose and prepare his spells in advance. To prepare or cast a
      spell, a ranger must have a Wisdom score equal to at least 10 + the spell
      level. The Difficulty Class for a saving throw against a ranger’s spell
      is 10 + the spell level + the ranger’s Wisdom modifier. Like other
      spellcasters, a ranger can cast only a certain number of spells of each
      spell level per day. His base daily spell allotment is given on Table:
      Ranger. In addition, he receives bonus spells per day if he has a high
      Wisdom score (see Table: Ability Modifiers and Bonus Spells). When Table:
      Ranger indicates that the ranger gets 0 spells per day of a given spell
      level, he gains only the bonus spells he would be entitled to based on
      his Wisdom score for that spell level. A ranger must spend 1 hour per day
      in quiet meditation to regain his daily allotment of spells. A ranger may
      prepare and cast any spell on the ranger spell list, provided that he can
      cast spells of that level, but he must choose which spells to prepare
      during his daily meditation. Through 3rd level, a ranger has no caster
      level. At 4th level and higher, his caster level is equal to his ranger
      level – 3.
  - name: Woodland Stride
    type: Ex
    levels:
      - 7
    text: |
      Starting at 7th level, a ranger may move through any sort of undergrowth
      (such as natural thorns, briars, overgrown areas, and similar terrain) at
      his normal speed and without taking damage or suffering any other
      impairment. Thorns, briars, and overgrown areas that are enchanted or
      magically manipulated to impede motion, however, still affect him.
  - name: Swift Tracker
    type: Ex
    levels:
      - 8
    text: |
      Beginning at 8th level, a ranger can move at his normal speed while using
      Survival to follow tracks without taking the normal –5 penalty. He takes
      only a –10 penalty (instead of the normal –20) when moving at up to twice
      normal speed while tracking.
  - name: Evasion
    type: Ex
    levels:
      - 9
    text: |
      When he reaches 9th level, a ranger can avoid even magical and unusual
      attacks with great agility. If he makes a successful Reflex saving throw
      against an attack that normally deals half damage on a successful save,
      he instead takes no damage. Evasion can be used only if the ranger is
      wearing light armor, medium armor, or no armor. A helpless ranger does
      not gain the benefit of evasion.
  - name: Quarry
    type: Ex
    levels:
      - 11
    text: |
      At 11th level, a ranger can, as a standard action, denote one target
      within his line of sight as his quarry. Whenever he is following the
      tracks of his quarry, a ranger can take 10 on his Survival skill checks
      while moving at normal speed, without penalty. In addition, he receives a
      +2 insight bonus on attack rolls made against his quarry, and all
      critical threats are automatically confirmed. A ranger can have no more
      than one quarry at a time and the creature’s type must correspond to one
      of his favored enemy types. He can dismiss this effect at any time as a
      free action, but he cannot select a new quarry for 24 hours. If the
      ranger sees proof that his quarry is dead, he can select a new quarry
      after waiting 1 hour.
  - name: Camouflage
    type: Ex
    levels:
      - 12
    text: |
      A ranger of 12th level or higher can use the Stealth skill to hide in any
      of his favored terrains, even if the terrain doesn’t grant cover or
      concealment.
  - name: Improved Evasion
    type: Ex
    levels:
      - 16
    text: |
      At 16th level, a ranger’s evasion improves. This ability works like
      evasion, except that while the ranger still takes no damage on a
      successful Reflex saving throw against attacks, he henceforth takes only
      half damage on a failed save. A helpless ranger does not gain the benefit
      of improved evasion.
  - name: Hide in Plain Sight
    type: Ex
    levels:
      - 17
    text: |
      While in any of his favored terrains, a ranger of 17th level or higher
      can use the Stealth skill even while being observed.
  - name: Improved Quarry
    type: Ex
    levels:
      - 19
    text: |
      At 19th level, the ranger’s ability to hunt his quarry improves. He can
      now select a quarry as a free action, and can now take 20 while using
      Survival to track his quarry, while moving at normal speed without
      penalty. His insight bonus to attack his quarry increases to +4. If his
      quarry is killed or dismissed, he can select a new one after 10 minutes
      have passed.
  - name: Master Hunter
    type: Ex
    levels:
      - 20
    text: |
      A ranger of 20th level becomes a master hunter. He can always move at
      full speed while using Survival to follow tracks without penalty. He can,
      as a standard action, make a single attack against a favored enemy at his
      full attack bonus. If the attack hits, the target takes damage normally
      and must make a Fortitude save or die. The DC of this save is equal to 10
      + 1/2 the ranger’s level + the ranger’s Wisdom modifier. A ranger can
      choose instead to deal an amount of nonlethal damage equal to the
      creature’s current hit points. A successful save negates this damage. A
      ranger can use this ability once per day against each favored enemy type
      he possesses, but not against the same creature more than once in a
      24-hour period.
text: |
  For those who relish the thrill of the hunt, there are only predators and
  prey. Be they scouts, trackers, or bounty hunters, rangers share much in
  common: unique mastery of specialized weapons, skill at stalking even the
  most elusive game, and the expertise to defeat a wide range of quarries.
  Knowledgeable, patient, and skilled hunters, these rangers hound man, beast,
  and monster alike, gaining insight into the way of the predator, skill in
  varied environments, and ever more lethal martial prowess. While some track
  man-eating creatures to protect the frontier, others pursue more cunning
  game—even fugitives among their own people.

  **Role:** Ranger are deft skirmishers, either in melee or at range, capable
  of skillfully dancing in and out of battle. Their abilities allow them to
  deal significant harm to specific types of foes, but their skills are
  valuable against all manner of enemies.

  **Alignment:** Any
