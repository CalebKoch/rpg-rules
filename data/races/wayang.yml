name: Wayang
sources:
  - pathfinder-roleplaying-game-advanced-race-guide
tags:
  - Uncommon
summary: |
  The small wayangs are creatures of the Plane of Shadow. They are so attuned
  to shadow that it even shapes their philosophy, believing that upon death
  they merely merge back into darkness. The mysteries of their shadowy
  existence grant them the ability to gain healing from negative energy as well
  as positive energy.
ability_scores: +2 Dex, +2 Int, --2 Wis
type: humanoid
subtypes:
  - wayang
size: Small
ages:
  adulthood: 40
  intuitive: +4d6
  self_taught: +5d6
  trained: +6d6
  middle_age: 100
  old: 150
  venerable: 200
  maximum: +1d100
height_and_weight:
  weight_modifier: ×1
  genders:
    - name: male
      base_height: 3'0"
      base_weight: 35 lb.
      height_modifier: +2d4
    - name: female
      base_height: 2'10"
      base_weight: 30 lb.
      height_modifier: +2d4
text: |
  The wayangs are a race of small supernatural humanoids who trace their
  ancestry to the Plane of Shadows. They are extremely gaunt, with pixielike
  stature and skin the color of deep shadow. Deeply spiritual, they follow a
  philosophy known as "The Dissolution," which teaches that in passing they may
  again merge into the shadow. They readily express their beliefs through
  ritual scarification and skin bleaching, marking their bodies with raised
  white dots in ornate spirals and geometric patterns. Shy and elusive, they
  live in small, interdependent tribes. Wayangs rarely associate with outsiders.
racial_traits:
  - name: Ability Scores
    text: |
      +2 Dexterity, +2 Intelligence, -2 Wisdom

      Wayang are nimble and cagey, but their perception of the world is clouded
      by shadows.
    hidden: true
  - name: Wayang
    text: Wayangs are humanoids with the wayang subtype.
    hidden: true
  - name: Small
    text: |
      Wayangs are Small creatures and gain a +1 size bonus to their AC, a +1
      size bonus on attack rolls, a --1 penalty on their CMB and to CMD, and a
      +4 size bonus on [Stealth](/skills/stealth.html) checks.
    hidden: true
  - name: Slow Speed
    text: Wayangs have a base speed of 20 feet.
    hidden: true
  - name: Darkvision
    text: Wayangs can see in the dark up to 60 feet.
  - name: Light and Dark
    type: Su
    text: |
      Once per day as an immediate action, a wayang can treat positive and
      negative energy effects as if she were an undead creature, taking damage
      from positive energy and healing damage from negative energy. This
      ability lasts for 1 minute once activated.
  - name: Lurker
    text: |
      Wayangs gain a +2 racial bonus on [Perception](/skills/perception.html)
      and [Stealth](/skills/stealth.html) checks.
  - name: Shadow Magic
    text: |
      Wayangs add +1 to the DC of any saving throws against spells of the
      shadow subschool that they cast. Wayangs with a Charisma score of 11 or
      higher also gain the following spell-like abilities: 1/day---[*ghost
      sound*](/spells/ghost-sound/), [*pass without
      trace*](/spells/pass-without-trace/), and
      [*ventriloquism*](/spells/ventriloquism/). The caster level for these
      effects is equal to the wayang's level. The DC for these spells is equal
      to 10 + the spell's level + the wayang's Charisma modifier.
  - name: Shadow Resistance
    text: |
      Wayangs get a +2 racial bonus on saving throws against spells of the
      shadow subschool.
  - name: Languages
    text: |
      Wayangs begin play speaking Common and Wayang. Wayangs with high
      Intelligence scores can choose from the following: any human language,
      Abyssal, Aklo, Draconic, Goblin, Infernal, Nagaji, Samsaran, and Tengu.
    hidden: true
alternate_traits:
  Dissolution's Child: |
    Once per day, you may change your appearance to look as if you were little
    more than a 4-foot-tall area of shadow. Your physical form still exists and
    you are not incorporeal---only your appearance changes. This works like
    [*invisibility*](spells/invisibility/), except it only lasts 1 round per
    level (maximum 5 rounds). This is a supernatural ability. This racial trait
    replaces shadow magic.
favored_class_bonus:
  Bard: |
    Add one spell known from the wizard's illusion school spell list. This
    spell must be at least one level below the highest spell level the bard can
    cast. The spell is treated as being one level higher, unless it is also on
    the bard spell list.
  Oracle: |
    Add one spell known from the wizard's illusion school spell list. This
    spell must be at least one level below the highest spell level the oracle
    can cast. That spell is treated as one level higher unless it is also on
    the oracle spell list.
  Sorcerer: |
    Add +1/2 point of damage to any illusion spells of the shadow subschool
    cast by the sorcerer.
  Summoner: |
    Add +1 skill rank to the summoner's eidolon.
