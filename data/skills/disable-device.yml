name: Disable Device
sources:
  - pathfinder-roleplaying-game-core-rulebook
ability_score: Dex
armor_check_penalty: true
trained_only: true
text: |
  If you're trained in this skill, you are skilled at disarming traps and
  opening locks. In addition, this skill lets you sabotage simple mechanical
  devices, such as catapults, wagon wheels, and doors.

  | Disable Device task  | Requires |      Time       | Retry |     DC     |
  |:---------------------|:--------:|:---------------:|:-----:|:----------:|
  | Disarm or rig device |   ---    | 1 round or more |  Yes  | 10 or more |
  | Open lock            |   ---    |     1 round     |  Yes  | 20 or more |

  **Disarm or Rig Device:** When disarming a trap or other device, the Disable
  Device check is made secretly, so that you don’t necessarily know whether
  you’ve succeeded.

  The DC depends on how tricky the device is. If the check succeeds, you
  disable the device. If it fails by 4 or less, you have failed but can try
  again. If you fail by 5 or more, something goes wrong. If the device is a
  trap, you trigger it. If you’re attempting some sort of sabotage, you think
  the device is disabled, but it still works normally.

  You also can rig simple devices such as saddles or wagon wheels to work
  normally for a while and then fail or fall off some time later (usually after
  1d4 rounds or minutes of use).

  To disarm a magic trap, you must have the trapfinding ability.

  | Task                        |    Time    |        DC        | Example                                                           |
  |:----------------------------|:----------:|:----------------:|:------------------------------------------------------------------|
  | Simple                      |  1 round   |        10        | Jam a lock                                                        |
  | Tricky                      | 1d4 rounds |        15        | Sabotage a wagon wheel                                            |
  | Difficult                   | 2d4 rounds |        20        | Disarm a trap, reset a trap                                       |
  | Extreme                     | 2d4 rounds |        25        | Disarm a complex trap, cleverly sabotage a clockwork device       |
  | Magic trap                  | 2d4 rounds | 25 + spell level | *Fire trap*, *glyph of warding*, *symbol*, *teleportation circle* |
  | Leave no trace of tampering |    ---     |        +5        | ---                                                               |

  **Open Lock:** As a full-round action, you can attempt to open a lock. The DC
  for opening a lock depends on its quality. If you do not have a set of
  thieves' tools, these DCs increase by 10.

  | Lock Quality | DC |
  |:-------------|:--:|
  | Simple       | 20 |
  | Average      | 25 |
  | Good         | 30 |
  | Superior     | 40 |
