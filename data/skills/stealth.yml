name: Stealth
sources:
  - pathfinder-roleplaying-game-core-rulebook
ability_score: Dex
armor_check_penalty: true
text: |
  If people are observing you using any of their precise senses (typically
  sight), you can't use Stealth against them. (For more information on precise
  senses, see [Perception](/skills/perception.html).) Effects such as
  [*blur*](/spells/blur/) and [*displacement*](/spells/displacement/), which
  leave a clear visual of the you within the perceiving character's vision,
  aren't sufficient to use Stealth, but a shadowy area or a curtain work
  nicely. The hide in plain sight class ability allows a creature to use
  Stealth while being observed and thus avoids this whole situation.

  You can move up to half your normal speed and use Stealth at no penalty. When
  moving at a speed greater than half but less than your normal speed, you take
  a --5 penalty. It's impossible to use Stealth while attacking, running, or
  charging.

  If your observers are momentarily distracted (such as by a Bluff check), you
  can attempt to use Stealth if you can get to an unobserved place of some
  kind. This check, however, is made at a --10 penalty because you have to move
  fast.

  Your Stealth check is opposed by the Perception check of anyone who might
  notice you. Creatures that fail to beat your Stealth check are either unaware
  of you or aware of your presence (see below) and treat you as if you had
  total concealment.

  **Breaking Stealth:** When you start your turn using Stealth, you can leave
  cover or concealment and remain unobserved as long as you succeed at a
  Stealth check and end your turn in cover or concealment. Your Stealth
  immediately ends after you make an attack roll, whether or not the attack is
  successful (except when sniping, as noted below).

  **Sniping:** If you've already successfully used Stealth at least 10 feet
  from your target, you can make one ranged attack and then immediately take a
  move action to use Stealth again. You take a --20 penalty on your Stealth
  check to maintain your obscured location.

  EDITORS NOTE: There is additional information in [Ultimate Intrigue](sources/pathfinder-roleplaying-game-ultimate-intrigue) and
  [Giant Hunters Handbook](sources/pathfinder-player-compation-giant-hunters-handbook)
  #TODO Add that information
