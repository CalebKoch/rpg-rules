# RPG Tools

This repo contains a number of tools that help me with creating characters when I play in [OGL](data/LICENSE.md) tabletop RPGs.

### Running the tool.

#### Environment Variables

This tool has a number of environment variables that effect how it is run. 

  - DEBUG if this value is equal to `True` it sets the Django DEBUG setting, having it show more verbose error messages. This is useful when developing, but should not be used if it's deployed for normal use.
  - EXTERNAL_BASE_URL Some endpoints of this project return markdown that is going to be rendered in places that mean the usual relative paths are not useful. Those endpoints try to turn the paths into an absolute path, by prepending this value to the relative path.
  - ALLOWED_HOSTS This is used by Django to determine which HOSTS it is suppose to serve. When using `DEBUG=True`, the default for this will include localhost, so it may not be needed, but when deploying to a different host, this value should be set to the relevant hostname. **NOTE** If this project is deployed in a docker-compose with [Rpg Chat Bot](https://gitlab.com/CalebKoch/rpg-chat-bot) And that accesses it by it's container name, it's container name will also need to be included here.
  - SECRET_KEY To be completely honest, I don't fully know what this value is used for, but I do know that it should be a secret, so add an environment variable to overwrite the default if you are running this project yourself

#### Docker

The current build of master is available as the latest tag of the 
registry.gitlab.com/calebkoch/rpg-tools registry, and can be launched with  
`docker container run -p 8000:8000 --env-file ./.env registry.gitlab.com/calebkoch/rpg-tools`

#### Running locally

If you are running to test some changes, and don't want to build a new container each time,
you can launch the project without using docker as well. 

You will need [python](https://www.python.org/downloads/) installed for your OS.

To install the dependencies, run `pip install -r requirements.txt`

After that, you should be able to run the project by running `python manage.py runserver`

