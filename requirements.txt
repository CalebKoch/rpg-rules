Markdown==3.3.6
PyYAML==5.3.1
asgiref==3.7.2
Django==5.0
Jinja2==3.0.3
python-dotenv==1.0.1
